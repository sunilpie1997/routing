import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute, ParamMap} from '@angular/router';
@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styles: [`
  
  `]
})
export class DepartmentListComponent implements OnInit {

  selected_id:number
  departments=[

    {"id":1,"name":"Angular"},
    {"id":2,"name":"Nodejs"},
    {"id":3,"name":"Django"},
  ]
  constructor(private router:Router,private route:ActivatedRoute) { }//dependency injection

  ngOnInit(): void {
this.route.paramMap.subscribe((params:ParamMap)=>{//receiving id from url
this.selected_id=parseInt(params.get('id'));
})
  }
  onSelect(department){
//this.router.navigate( ['/departments',department.id])                /*navigating to '/departments/{id}'*/
this.router.navigate([department.id],{relativeTo:this.route})        /*relative routing*/
  }

  isSelected(department){
    return this.selected_id===department.id//strict equality check-->2=="2" return true because "2" is converted
    //"2"===2 returns false
  }
}
