import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DepartmentDetailComponent } from './department-detail/department-detail.component';
import { DepartmentOverviewComponent } from './department-overview/department-overview.component';



const routes: Routes = [
  {path:'',redirectTo:'/department-list',pathMatch:'full'},
  {path:'department-list/:id',component:DepartmentDetailComponent
,children:[
  {path:'overview',component:DepartmentOverviewComponent}//child routes
]
},
  {path:'department-list',component:DepartmentListComponent},
  {path:'employees',  component:EmployeeListComponent},
  {path:'**',component:PageNotFoundComponent},//always at end because router matches in order
];//all possible routes(each is a object)

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
//good practise to have all components (used in routing ) under hood 
export const RoutingComponents=[EmployeeListComponent,
                              DepartmentListComponent,
                              PageNotFoundComponent,
                               DepartmentDetailComponent,
                               DepartmentOverviewComponent]