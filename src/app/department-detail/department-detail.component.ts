import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Router} from '@angular/router';
@Component({
  selector: 'app-department-detail',
  template: `
    <p>
      <button (click)="goPrevious()">Previous</button>department selected with id={{department_id}}!
      <button (click)="goNext()">Next</button>
    </p>
    <p><button (click)="goBack()">Back</button></p>

    <p><button (click)="goToOverview()">Overview</button></p>

    <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class DepartmentDetailComponent implements OnInit {

  department_id :number
  
  constructor(private route:ActivatedRoute,private router:Router){}

  ngOnInit(): void {
   // let id=parseInt(this.route.snapshot.paramMap.get('id'));same component is shown when 
   //previous and next button are clicked ,no initialisation of new component ('ngOnInit()' is not called) 
   this.route.paramMap.subscribe((params:ParamMap)=>{
     let id=parseInt(params.get('id'));
     this.department_id=id;
   });
  }

  goPrevious(){
    if(this.department_id>1){
let previous_id=this.department_id-1;
//this.router.navigate(['departments/',previous_id]);
this.router.navigate(['../',previous_id],{relativeTo:this.route});
    }
  }

  goNext(){
    if(this.department_id<2){
    let next_id=this.department_id+1;
    //this.router.navigate(['departments/',next_id]);
    this.router.navigate(['../',next_id],{relativeTo:this.route});
    }
      }

      goBack(){
        let selected_id=this.department_id?this.department_id:null//null check
        this.router.navigate(['../',{id:this.department_id}],{relativeTo:this.route})//optional route parameters-->no need to configure routes table
      }
      goToOverview(){
        this.router.navigate(['overview'],{relativeTo:this.route});
      }
}
